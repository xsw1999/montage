
import { getCircularPos } from '../common.js'

export default class Circular {
  constructor(ctx){
    this.Ctx = ctx;
  }

  // 绘制圆形
  circular(info,save = true,reload = true){
    this.Ctx.ctx.save()
    if(info.auxProps) {
      for(let [k, v] of Object.entries(info.auxProps)) {
        this.Ctx.ctx[k] = v;
      }
    }
    let {x,y,id = new Date().getTime(),radiusX = 10, radiusY = 10, startAngle = 0, endAngle = Math.PI * 2, anticlockwise = true,rotate = 0,lineWidth,fillColor,fillType='stroke',textInfo, level = 1} = info
    if(reload) this.Ctx.repaint();
    this.Ctx.rotate = rotate;
    this.Ctx.ctx.save();
    this.Ctx.ctx.beginPath();
    let ele = {};
    this.Ctx.ctx.lineWidth = lineWidth || 1;
    this.Ctx.Custom.setColor({fillColor, fillType})
    this.Ctx.ctx.translate(x, y);   // 重新定义当前坐标系原点
    this.Ctx.ctx.rotate(rotate);
    // this.Ctx.ctx.arc(startX, startY, radius, startAngle, endAngle, anticlockwise);
    
    // 改为：使用圆的方程绘制圆形（更方便绘制椭圆）
    this.circularEquation({radiusX,radiusY,startAngle,endAngle});
    
    if(fillType === 'stroke'){
      this.Ctx.ctx.stroke();
    }else{
      this.Ctx.ctx.fill();
    }
    this.Ctx.ctx.restore();
    
    let copyX = x;
    let copyY = y;
    if(save) {
      ele = {...info, id,type:'circular',x:copyX,y:copyY,radiusX, radiusY, startAngle, endAngle, anticlockwise,rotate,textInfo,fillColor,fillType,lineWidth,level}
      this.Ctx.canvasInfoMap.set(id, ele)
    }
    this.Ctx.ctx.restore()
    return ele;
  }
  
  // 使用圆形方程进行绘制
  circularEquation({ctx = this.Ctx.ctx, startX = 0, startY = 0, radiusX, radiusY, startAngle, endAngle}){
    let pos = getCircularPos({startX, startY, radiusX, radiusY, startAngle, endAngle})
    ctx.moveTo(...pos[0]);
    pos.forEach((i) => {
      ctx.lineTo(...i);
    })
    if((endAngle - startAngle) !== 2*Math.PI) ctx.lineTo(startX,startY)
    ctx.closePath();
  }
}