
import Sign from '../sign/sign.js'
import Rect from './rect.js'
import Line from './line.js'
import Circular from './circular.js'

export default class Shape extends Sign{
  constructor(){
    super();

    this.Rect = new Rect(this);
    this.Line = new Line(this);
    this.Circular = new Circular(this);
  }
}