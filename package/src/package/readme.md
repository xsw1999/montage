* canvasInfo 格式 -> 
````js
[ { 

  id: new Date().getTime(), 
  name: '', 
  type: '',      // 类型

  x: '',         // 坐标起止点
  y: '', 
  endX: '', 
  endY: '', 

  minX: '',      // 最小的x（确定点击范围）
  maxX: '',
  minY: '',
  maxY: '',

  width: '',    // 宽度（绘制矩形）
  height: '',   // 高度（绘制矩形）

  info: {},     // 详细信息（如配置等）
  
} ]
````