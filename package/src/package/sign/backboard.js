import Behavior from '../behavior'

export default class Backboard extends Behavior {
  constructor() {
    super()
    this.rgbMap = new Map();
  }

  // ***************************** 以下是背景板（保存不同图案的状态） ****************************
  // 创建一块新的画布
  createBackboard(width,height){
    let backboard = document.createElement('canvas');
    backboard.id = 'backboard';
    backboard.width = width;
    backboard.height = height;
    backboard.style.width = 0 + 'px';
    backboard.style.height = 0 + 'px';
    backboard.style.position = 'absolute';
    document.body.appendChild(backboard);
    let canvas = document.getElementById("backboard");
    canvas.width = width;
    canvas.height = height;
    if (canvas.getContext) {
      if (!this.backboard) this.backboard = canvas.getContext("2d");
    }
  }
  // 随机生成颜色值
  getRgb(){
    for(let b = 1;b<=255;b++){
      if(!this.rgbMap.get(`${this.r},${this.g},${b}`)){
        this.rgbMap.set(`${this.r},${this.g},${b}`, true);
        if(b === 255) this.g ++;
        if(this.g === 255) {
          this.r ++;
          this.g = 0;
        }
        if(this.r === 256){
          throw new Error('元素已达上限')
        }
        return `rgb(${this.r},${this.g},${b})`
      }
    }
  }
  // 重新绘制背景板
  repaintBackboard(){
    this.rgbMap = new Map();
    this.r = 0;
    this.g = 0;
    this.boardCanvas = {};
    this.boardSign = {};
    this.backboard.clearRect(0,0,this.canvas.width, this.canvas.height);
    if(this.canvasInfoMap.size){
      this.canvasInfoMap.forEach((i) => {
        let fillStyle = this.getRgb();
        this.boardCanvas[fillStyle] = i.id;
        if(['rect', 'image'].includes(i.type)){
          this.boardRect(i,fillStyle)
        }else if(i.type === 'line'){
          this.boardLine(i,fillStyle)
        }else if(i.type === 'circular'){
          this.boardCircular(i,fillStyle)
        }
      })
      for(let i of [...this.sign, ...this.linkArr]){
        let fillStyle = this.getRgb();
        this.boardSign[fillStyle] = i;
        this.borderSignRect(i,fillStyle)
      }
    }    
  }
  // 绘制矩形
  boardRect({x, y, width = 10,height = 10,rotate = 0},fillStyle) {
    this.backboard.fillStyle = fillStyle;
    this.backboard.save();
    this.backboard.translate(x+width/2, y+height/2);   // 重新定义当前坐标系原点
    this.backboard.rotate(rotate);
    this.backboard.fillRect(-width/2, -height/2, width, height);  // 此处不用xy作为原点直接绘制，当前坐标系原点已被重新定义
    this.backboard.restore();
  }
  // 绘制线
  boardLine({x,y,endX,endY},fillStyle){
    this.backboard.fillStyle = fillStyle;
    this.backboard.lineWidth = 10;
    this.boardLineBegin({x,y});
    return this.boardLineTo({endX,endY});
  }
  boardLineBegin({x,y}){
    this.backboard.beginPath(x,y);
    this.backboard.moveTo(x,y);
  }
  boardLineTo({endX,endY}){
    this.backboard.lineTo(endX,endY);
    this.backboard.closePath();
    this.backboard.stroke();
  }
  // 绘制圆形
  boardCircular(info,fillStyle){
    let {x,y,radiusX, radiusY,rotate = 0, startAngle, endAngle} = info
    this.backboard.save();
    this.backboard.beginPath();
    this.backboard.fillStyle = fillStyle;
    this.backboard.translate(x, y);   // 重新定义当前坐标系原点
    this.backboard.rotate(rotate);
    // this.backboard.arc(0,0, radius, startAngle, endAngle, anticlockwise);
    this.Circular.circularEquation({ctx:this.backboard,radiusX,radiusY,startAngle, endAngle});
    this.backboard.fill();
    this.backboard.restore();
  }
  // 绘制标记点
  borderSignRect({signX,signY,signWidth,signHeight,rotate = 0,signType = 'zoom',centerX=this.centerTarget.x,centerY=this.centerTarget.y},fillStyle){
    this.backboard.fillStyle = fillStyle;
    if(['zoom','rotate','connect'].includes(signType)){
      this.backboard.save();
      this.backboard.translate(centerX, centerY);   // 重新定义当前坐标系原点
      this.backboard.rotate(rotate);
      if(signType === 'connect'){
        this.backboard.beginPath();
        this.backboard.arc(signX+signWidth/2, signY+signHeight/2, 9, 0, Math.PI*2, true);
        this.backboard.fill();
      }else{
        this.backboard.fillRect(signX, signY, signWidth, signHeight);
      }
    }else{
      // 直线不需要旋转坐标
      this.backboard.fillRect(signX, signY, signWidth, signHeight);
    }
    this.backboard.restore();
  }

}