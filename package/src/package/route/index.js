import Curve from "./curve.js";

// 连接线方法（传入两个图形坐标，只返回连接线的数据格式）
export default class Connect {
  constructor(ctx) {
    this.Curve = new Curve(ctx);
  }

  // 直线（示例）
  line({ startX, startY, targetX, targetY, startId = null, targetId = null }) {
    return {
      type: "line",
      startId,
      targetId,
      startX,
      startY,
      targetX,
      targetY,
      linkId: this.Draw.getUniqueId(),
    };
  }

  /*
    曲线方法
  */
  // 绘制曲线
  drawThreeCurve(val) {
    return this.Curve.drawThreeCurve(val);
  }
  // 获取曲线坐标
  getThreeCurve({ startSign, targetSign, type }, view) {
    return this.Curve.getThreeCurve({ startSign, targetSign, type }, view);
  }
}
