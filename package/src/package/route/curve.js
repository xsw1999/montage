import { getReal } from "../common.js";

export default class Curve {
  constructor(ctx) {
    this.Curve = ctx;
  }

  // 绘制三次方贝塞尔曲线
  drawThreeCurve({ fillColor, x, y, x1, y1, x2, y2, endX, endY, lineWidth }) {
    this.Curve.ctx.strokeStyle = fillColor || "rgb(0,0,0)";
    // this.Curve.ctx.lineWidth = 1;

    /* 连接线的宽度暂时不支持调整（因为涉及到箭头曲线的计算问题）*/
    /* 2024.11.26 上面 👆 这个问题不考虑了 */
    this.Curve.ctx.lineWidth = lineWidth || 1;
    this.Curve.ctx.beginPath();
    this.Curve.ctx.moveTo(x, y);
    this.Curve.ctx.closePath();
    this.Curve.ctx.bezierCurveTo(x1, y1, x2, y2, endX, endY);
    this.Curve.ctx.stroke();
  }

  // *************** 获取曲线的坐标 ***************
  // 三次贝塞尔曲线
  // 传入需要连接的两个连接点元素
  getThreeCurve({ startSign, targetSign, type }) {
    let res = {
      startInfo: {
        id: startSign.id,
        pos: startSign.pos,
        rotate: startSign.rotate || 0,
      },
      targetInfo: {
        id: targetSign.id,
        pos: targetSign.pos,
        rotate: targetSign.rotate || 0,
      },
      x: startSign.signCenterX,
      y: startSign.signCenterY,
      endX: targetSign.signCenterX,
      endY: targetSign.signCenterY,
      x1: 0,
      y1: 0,
      x2: 0,
      y2: 0,
    };
    let ruler = 40;

    // 分别处理两个图形的偏移点
    if (startSign.pos === "1") {
      let start = getReal({ x: 0, y: -ruler, rotate: startSign.rotate });
      res.x1 = startSign.signCenterX - start.realX;
      res.y1 = startSign.signCenterY + start.realY;
    } else if (startSign.pos === "2") {
      let start = getReal({ x: ruler, y: 0, rotate: startSign.rotate });
      res.x1 = startSign.signCenterX + start.realX;
      res.y1 = startSign.signCenterY - start.realY;
    } else if (startSign.pos === "3") {
      let start = getReal({ x: 0, y: ruler, rotate: startSign.rotate });
      res.x1 = startSign.signCenterX - start.realX;
      res.y1 = startSign.signCenterY + start.realY;
    } else if (startSign.pos === "4") {
      let start = getReal({ x: -ruler, y: 0, rotate: startSign.rotate });
      res.x1 = startSign.signCenterX + start.realX;
      res.y1 = startSign.signCenterY - start.realY;
    }

    if (targetSign.pos === "1") {
      let target = getReal({ x: 0, y: -ruler, rotate: targetSign.rotate });
      res.x2 = targetSign.signCenterX - target.realX;
      res.y2 = targetSign.signCenterY + target.realY;
    } else if (targetSign.pos === "2") {
      let target = getReal({ x: ruler, y: 0, rotate: targetSign.rotate });
      res.x2 = targetSign.signCenterX + target.realX;
      res.y2 = targetSign.signCenterY - target.realY;
    } else if (targetSign.pos === "3") {
      let target = getReal({ x: 0, y: ruler, rotate: targetSign.rotate });
      res.x2 = targetSign.signCenterX - target.realX;
      res.y2 = targetSign.signCenterY + target.realY;
    } else if (targetSign.pos === "4") {
      let target = getReal({ x: -ruler, y: 0, rotate: targetSign.rotate });
      res.x2 = targetSign.signCenterX + target.realX;
      res.y2 = targetSign.signCenterY - target.realY;
    }

    if (type === "connectArrowCurveFill") {
      // 添加连接箭头
      Object.assign(res, this.arrow(res));
    }

    res.type = type;

    return res;
  }

  // 计算连接线箭头的方法（根据最后两个点的坐标来决定箭头指向）
  arrow({ endX, endY, targetInfo }) {
    // 根据目标图案的连接点计算临时斜率（x坐标为0度）
    let rotate;
    let radian = Math.atan2(1, 0); // 90度对应的弧度
    if (targetInfo.pos === "1") {
      rotate = targetInfo.rotate + 3 * radian;
    } else if (targetInfo.pos === "2") {
      rotate = targetInfo.rotate;
    } else if (targetInfo.pos === "3") {
      rotate = targetInfo.rotate + 1 * radian;
    } else if (targetInfo.pos === "4") {
      rotate = targetInfo.rotate + 2 * radian;
    }

    let sideLength = 8; // 三角形斜边长

    // 记录三角形底部的中心点（不是真实坐标，需要加上初始的坐标点）
    let tabX;
    let tabY;
    tabX = Math.cos(rotate) * sideLength;
    tabY = Math.sin(rotate) * sideLength;

    // 记录三角形底部的两个偏移坐标（使用的时候用底部中心点坐标分别 +/- 偏移量，即可添加）
    let shiftK = rotate + radian; // 底边弧度，垂直于图案原本的弧度，所以要 +1
    let shiftX;
    let shiftY;
    let sub = (sideLength / 4) * 3; // 比较合适的底边距离
    shiftX = sub * Math.cos(shiftK);
    shiftY = sub * Math.sin(shiftK);

    let lineWay = []; // 存储图案最终的点
    let one = { x: endX, y: endY };

    // 计算第二个点的位置，中心点的一侧
    let two = [tabX + endX - shiftX, tabY + endY - shiftY];
    lineWay.push(two);

    // 计算第三个点的位置，中心点的另一侧
    let three = [tabX + endX + shiftX, tabY + endY + shiftY];
    lineWay.push(three);

    return { start: one, lineWay };
  }
}
