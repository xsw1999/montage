
// 画布使用的节点例子
const boardExample = [
  {
    zoom: true,
    connect: true,
    level: 2,
    type: "rect",
    x: 150,
    y: 150,
    width: 60,
    height: 60,
    rotate: 0,
    fillColor: "rgb(0,200,0)",
    fillType: "fill",
    lineWidth: 5,
    textInfo: {
      text: "层级：2",
    },
  },
  {
    level: 3,
    type: "rect",
    x: 400,
    y: 100,
    width: 60,
    height: 60,
    fillColor: "rgb(200,0,0, 0.2)",
    fillType: "fill",
    lineWidth: 5,
    textInfo: {
      text: "层级：3",
    },
  },
  { type: "rect", x: 400, y: 200, width: 60, height: 60 },
  {
    type: "line",
    zoom: true,
    connect: true,
    x: 100,
    y: 200,
    endX: 200,
    endY: 10,
    fillColor: "rgb(200,0,0)",
    lineWidth: 5,
    level: 3,
  },
  {
    type: "rect",
    zoom: true,
    connect: true,
    x: 100,
    y: 150,
    width: 80,
    height: 40,
    // rotate: 0.3,
    fillColor: "rgba(200, 0, 0, 1)",
    textInfo: {
      text: "hello world",
    },
  },
  {
    type: "image",
    zoom: true,
    connect: true,
    level: 1,
    x: 220,
    y: 200,
    fillColor: 'rgba(0, 0, 0, 0)',
    width: 140,
    height: 150,
    rotate: 0.5,
    src: require("../assets/logo.png"),
    textInfo: {
      text: "层级：1",
    },
  },
  {
    type: "circular",
    x: 200,
    y: 70,
    drag: false,
    radiusX: 50,
    radiusY: 30,
    textInfo: {
      text: "禁止拖拽",
      fillColor: "blue",
    },
  },
]

// 动效例子
const effectExample = [
  {
    startX: 10,
    startY: 50,
    endX: 280,
    endY: 50,
  },
  {
    startX: 100,
    startY: 50,
    endX: 200,
    endY: 200,
  },
  {
    startX: 280,
    startY: 200,
    endX: 10,
    endY: 200,
  }
]

export { boardExample as nodes,  effectExample as routes}