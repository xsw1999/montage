import { ElMessage } from 'element-plus'
import Flow from "../package/index.js";
// import Flow from 'montage.js'

// 初始化
let draw = null;
const init = ({width = 600, height = 600, defaultStyle, drag = true }, dom = document.getElementById('drawingBoard'), checked) => {
  draw = new Flow({
    drag,
    width,
    height,
    defaultStyle,
    highlight: {
      fillColor: "rgba(0,200,200)",
    }
  }, dom, checked);
  return draw
}

// 添加节点
const setNode = (type) => {
  ElMessage.info("请在画布中绘制");
  draw.setNodeType(type);
}

// 移除节点
const removeNode = (id) => {
  draw.deleteNode(id)
}

// 清空画布
const clear = () => {
  draw.clearable();
};

// 添加图片
const loadImage = () => {
  // clear();
  draw.addNode({
    type: "image",
    zoom: true,
    connect: true,
    x: 250,
    y: 100,
    level: 0,
    fillColor: "rgba(0, 0, 0, 0)",
    width: 140,
    height: 150,
    rotate: 0.8,
    src: require("../assets/logo.png"),
    textInfo: {
      text: "层级：0",
    },
  });
};

// 饼图方法
const pie = () => {
  clear();
  let arr = [50, 20, 30];
  let colors = ["rgba(200,0,0,0.5)", "rgba(0,200,0,0.5)", "rgba(0,0,200,0.5)"];
  const sum = arr.reduce((a, b) => a + b);
  let sub = sum;
  let startAngle = 0;
  let endAngle = 2 * Math.PI;
  let obj = {
    type: "circular",
    x: 200,
    y: 200,
    startAngle,
    endAngle,
    radiusX: 70,
    radiusY: 70,
    fillType: "fill",
    fillColor: "rgba(200,0,0,0.5)",
    textInfo: {},
  };
  arr.forEach((i, v) => {
    obj.startAngle = ((sum - sub) / sum) * 2 * Math.PI;
    sub -= i;
    obj.endAngle = ((sum - sub) / sum) * 2 * Math.PI;
    obj.fillColor = colors[v];
    console.log(i / sum);
    obj.textInfo.text = (i / sum) * 100 + "%";
    obj.a = obj.textInfo.text;
    draw.addNode(obj);
  });
};

// 柱状图
const bar = () => {
  clear();
  // 定义两组数据，分别是数值/名称
  let arr = [50, 30, 10, 70, 40, 90, 70];
  let legend = ["Mon", "Tue", "Wed", "Thu", "Fri", "Sat", "Sun"];
  let width = 45; // 定义柱体宽度
  let interval = 20; // 定义柱体间隔
  let start = { x: 100, y: 150 }; // 定义图表开始的点
  let maxHeight = Math.max(...arr); // 找到矩形最大值（方便计算矩形离开始点的距离）
  for (let i = 0; i < arr.length; i++) {
    let rect = {
      connect: true,
      x: start.x + i * (width + interval), // 动态计算每个矩形的起点（x, y)
      y: start.y + maxHeight - arr[i],
      width: width,
      height: arr[i], // 高度直接作为数值
      type: "rect",
      fillType: "fill",
      fillColor: "rgb(80,155,195)", // 定义矩形颜色
    };
    draw.addNode(rect);
    let text = {
      level: 2,
      type: "rect",
      x: start.x + i * (width + interval), // 动态计算每个文本的起点（x, y)
      y: start.y + maxHeight,
      width: width,
      height: 30,
      fillColor: "white",
      textInfo: {
        text: legend[i], // 输入文本
      },
    };
    draw.addNode(text);
  }
  let line = {
    type: "line",
    x: start.x - interval, // 计算线的起点位置 x, y（矩形底端）
    y: start.y + maxHeight,
    endX: start.x + arr.length * (width + interval), // 计算线的终点位置 x, y（矩形底端）
    endY: start.y + maxHeight,
    fillColor: "rgb(110,112,121)",
    lineWidth: 1,
  };
  draw.addNode(line);
  console.log(draw)
};

// 绘制自定义图案
const drawCanvas = (val) => {
  let model = {
    rect: {
      start: {
        x: 400,
        y: 200,
      },
      lineWay: [
        [500, 200],
        [500, 300],
        [400, 300],
        [400, 200],
      ],
      fillType: "stroke",
    },
    star: {
      start: {
        x: 200,
        y: 200,
      },
      lineWay: [
        [230, 100],
        [260, 200],
        [360, 230],
        [260, 260],
        [230, 360],
        [200, 260],
        [100, 230],
        [200, 200],
      ],
      fillType: "stroke",
      fillColor: "rgba(200,200,100,0.8)",
    },
  };
  draw.customDrawing(model[val]);
};

export { init, setNode, removeNode, pie, bar, clear, drawCanvas, loadImage }