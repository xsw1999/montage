import {createRouter, createWebHashHistory} from 'vue-router';

const route = [{
  path: '/',
  redirect: '/home'
},{
  path: '/home',
  name: '首页', 
  component: () => import('@/home/Index.vue')
},{
  path: '/drawing-board',
  name: '画板',
  component: () => import('@/home/board/Index.vue')
},{
  path: '/dimension',
  name: '标记示例',
  component: () => import('@/home/dimension/Index.vue')
},{
  path: '/effect',
  name: '动画示例',
  component: () => import('@/home/effect/Index.vue')
},{
  path: '/drag',
  name: '画布缩放/拖拽示例',
  component: () => import('@/home/drag/Index.vue')
},{
  path: '/share',
  name: '分享图片示例',
  component: () => import('@/home/share/Index.vue')
},{
  path: '/connect',
  name: '节点间连线',
  component: () => import('@/home/connect/Index.vue')
},{
  path: '/screenshot',
  name: '截图',
  component: () => import('@/home/screenshot/Index.vue')
}]

const router = createRouter({
  history: createWebHashHistory(''),
  routes: route,
  strict: true
})

export default router;