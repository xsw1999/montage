
// 画布使用的节点例子
const boardExample = [
  {
    zoom: true,
    connect: true,
    level: 2,
    type: "rect",
    x: 150,
    y: 150,
    width: 60,
    height: 60,
    rotate: 0,
    fillColor: "rgb(0,200,0)",
    fillType: "fill",
    lineWidth: 5,
    textInfo: {
      text: "层级：2",
    },
  },
  {
    level: 3,
    type: "rect",
    x: 400,
    y: 100,
    width: 60,
    height: 60,
    fillColor: "rgb(200,0,0, 0.2)",
    fillType: "fill",
    lineWidth: 5,
    textInfo: {
      text: "层级：3",
    },
  },
  { type: "rect", x: 400, y: 200, width: 60, height: 60 },
  {
    type: "line",
    zoom: true,
    connect: true,
    x: 100,
    y: 200,
    endX: 200,
    endY: 10,
    fillColor: "rgb(200,0,0)",
    lineWidth: 5,
    level: 3,
  },
  {
    type: "rect",
    zoom: true,
    connect: true,
    x: 100,
    y: 150,
    width: 80,
    height: 40,
    // rotate: 0.3,
    fillColor: "rgba(200, 0, 0, 1)",
    textInfo: {
      text: "hello world",
    },
  },
  {
    type: "image",
    zoom: true,
    connect: true,
    level: 1,
    x: 220,
    y: 200,
    fillColor: 'rgba(0, 0, 0, 0)',
    width: 140,
    height: 150,
    rotate: 0.5,
    src: new URL("../assets/logo.png", import.meta.url).href,
    textInfo: {
      text: "层级：1",
    },
  },
  {
    type: "circular",
    x: 200,
    y: 70,
    drag: false,
    radiusX: 50,
    radiusY: 30,
    textInfo: {
      text: "禁止拖拽",
      fillColor: "blue",
    },
  },
]

// 动效例子
const effectExample = [
  {
    startX: 10,
    startY: 50,
    endX: 280,
    endY: 50,
  },
  {
    startX: 100,
    startY: 50,
    endX: 200,
    endY: 200,
  },
  {
    startX: 280,
    startY: 200,
    endX: 10,
    endY: 200,
  }
]

const shareExample = [
  {
    type: 'rect',
    x: 0,
    y: 0,
    width: 370,
    height: 570,
    fillColor: 'white',
    fillType: 'fill',
    level: 0
  }, 
  {
    type: 'rect',
    x: 0,
    y: 0,
    width: 370,
    height: 60,
    fillColor: 'rgba(0, 0, 0, 0)',
    textInfo: {
      text: 'TITLE',
      font: "20px serif",
    }
  },  
  {
    type: 'rect',
    x: 35,
    y: 60,
    width: 300,
    height: 380,
    fillColor: 'rgb(214, 213, 213)',
    lineWidth: 1,
    textInfo: {
      text: '封面图',
      font: "23px serif",
    }
  },
  {
    type: 'rect',
    x: 35,
    y: 455,
    width: 200,
    height: 100,
    fillColor: 'rgba(0, 0, 0, 0)',
    textInfo: {
      text: '文字说明/n文字说明  文字说明 ... .../n这里是文字说明  这里是文字说明~~~/n最后能否帮我点个星星🧐🧐🧐/n多谢  ~~  🥳🥳🥳',
      font: "11px serif",
    }
  },
  {
    type: "image",
    x: 235,
    y: 455,
    width: 100,
    height: 100,
    src: new URL('@/assets/share/code.png', import.meta.url).href
  }
]

const connectNodes = [  
  {
    id: '1',
    type: "rect",
    x: 20,
    y: 50,
    width: 80,
    height: 40,
    textInfo: {
      text: "hello world",
    },
  },
  {
    id: '2',
    type: "circular",
    x: 200,
    y: 70,
    radiusX: 50,
    radiusY: 30,
    textInfo: {
      text: "hello world",
    },
  }
]
const connectExample = [  
  {
    type: "connectCurve",
    startInfo: {
        id: '1',
        pos: "3",
    },
    targetInfo: {
        id: '2',
        pos: "3",
    },
  }, {
    type: "connectArrowCurveFill",
    startInfo: {
        id: '1',
        pos: "1",
    },
    targetInfo: {
        id: '2',
        pos: "4",
    },
  }
]

export { boardExample as nodes,  effectExample as routes, shareExample, connectNodes, connectExample}