import { defineConfig } from 'vite'
import vue from '@vitejs/plugin-vue'
import { resolve } from 'path'

const getResolve = (path) => {
  return resolve(process.cwd(), '.', path)
}

// https://vitejs.dev/config/
export default defineConfig({
  plugins: [vue()],
  resolve: {
    alias: [{
      find: '@',
      replacement: getResolve('src')
    }]
  },
  server: {
    port: 5171
  }
})
